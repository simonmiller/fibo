﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


class Fibo
{
    private static int[] arr = new int[1000];

    public static int Find(int n)
    {
        arr[0] = 0; //Не понял,как инициализировать один раз.
        arr[1] = 1; //Поэтому такая фигня.Но работает всё равно шустро.Не бей меня сильно.
        for(int i = 0;i < arr.Length; i++)
        {
            if (arr[i]==n)
            {
                return n;
            }
        }
        arr[n] = Find(n - 2) + Find(n - 1);
        return arr[n];
    }
}

class FiboDemo
{
    static void Main()
    {
        while (true)
        {
            var watch = new Stopwatch();
            var n = int.Parse(Console.ReadLine());

            watch.Start();
            Console.WriteLine("Value: " + Fibo.Find(n));
            watch.Stop();
            Console.WriteLine("Elased: " + watch.Elapsed);
        }
    }
}